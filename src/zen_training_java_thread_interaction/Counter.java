package zen_training_java_thread_interaction;

public class Counter {
	final static int FUNCTION_LOCKER = 0;
	final static int FUNCTION_COUNTER = 1;
	static int count;
	static boolean isLocked;
	static boolean enableLocking = false;
	private int mFunction;
	
	public Counter(int function) {
		mFunction = function;
	}
	
	synchronized void incrementCount() {
		for(int i = 0; i<15; i++) {
			count += 1;
			System.out.println("Count: " + count);
			//notify();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			try {
				while(isLocked) { 
					System.out.println("Thread is locked");
					wait();
					System.out.println("Thread is unlocked");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	synchronized void lock() {
		isLocked = true;
	}
	
	synchronized void blockCounter() {
		isLocked = true;
		System.out.println("locked");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		isLocked = false;
		System.out.println("unlocked");
		notify();
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	synchronized void unlock() {
		isLocked = false;
		System.out.println("unlocked");
		notify();
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public int getFunction() {
		return mFunction;
	}
}
