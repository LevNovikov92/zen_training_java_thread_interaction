package zen_training_java_thread_interaction;

public class CounterThread implements Runnable {
	private Counter mCounter;
	
	public CounterThread(Counter counter) {
		mCounter = counter;
	}
	
	@Override
	public void run() {
		if(mCounter.getFunction() == Counter.FUNCTION_COUNTER) {
				mCounter.incrementCount();
		} else {
			try {
				Thread.sleep(1000);
				mCounter.blockCounter();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
