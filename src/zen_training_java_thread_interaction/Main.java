package zen_training_java_thread_interaction;

import java.awt.event.MouseWheelEvent;

import printer.MessagePrinter;
import printer.Queue;

public class Main {

	public static void main(String[] args) {
		//startThreadInteractionTest();
		//startWaitNotifyTest();
		startLockTest();
	}

	private static void startLockTest() {
		Queue queue = new Queue();
		Thread threads[] = new Thread[10];
		
		for(int i=0 ; i<threads.length; i++) {
			threads[i] = new Thread(new MessagePrinter("message from thread "+i, queue));
		}
		
		for(int i=0 ; i<threads.length; i++) {
			threads[i].start();
		}
	}

	private static void startWaitNotifyTest() {
		ComputingThread computationThread = new ComputingThread();
		computationThread.start();
		
		synchronized (computationThread) {
			System.out.println("Computation Thread started...");
			try {
				computationThread.wait();
				System.out.println("Result:  " + computationThread.getResult());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void startThreadInteractionTest() {
		Counter counter = new Counter(Counter.FUNCTION_COUNTER);
		Counter locker = new Counter(Counter.FUNCTION_LOCKER);
		
		Thread counterThread = new Thread(new CounterThread(counter));
		Thread lockerThread = new Thread(new CounterThread(locker));
		counterThread.start();
		lockerThread.start();
		
		try {
			lockerThread.join();
			counterThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
