package printer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue {
	private final Lock queueLock = new ReentrantLock();
	
	public void getResult(String message) {
		queueLock.lock();
		String threadName = Thread.currentThread().getName(); 
		System.out.println("Thread "+threadName+"; Message: " + message);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		queueLock.unlock();
	}
}
