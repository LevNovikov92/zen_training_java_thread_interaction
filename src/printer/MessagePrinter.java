package printer;

public class MessagePrinter implements Runnable {
	private Queue mQueue;
	private String mMessage;
	
	public MessagePrinter(String message, Queue queue) {
		mMessage = message;
		mQueue = queue;
	}

	@Override
	public void run() {
		mQueue.getResult(mMessage);
	}
}
